import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;
public class Questions {
    Scanner scanner = new Scanner(System.in);
    public ArrayList<String> listOfQuestions = new ArrayList<String>();
    public ArrayList<Integer> listOfAnswers = new ArrayList<Integer>();
    public ArrayList<ArrayList<String>> listOfPossibleAnswers = new ArrayList<ArrayList<String>>();
    public ArrayList<Integer> listOfCosts = new ArrayList<>();


    public void addQuestion(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter question");
        String question = scanner.nextLine();
        ArrayList<String> bufferedList = new ArrayList<>();


        int i = 0;
        System.out.println("Now enter possible answers. To finish adding them enter '-1'");
        System.out.print(i + ". ");
        String possibleAnswer = scanner.nextLine();
        while (!possibleAnswer.equals("-1")){
            i++;
            System.out.print(i + ". ");
            bufferedList.add(possibleAnswer);
            possibleAnswer = scanner.nextLine();

        }
        System.out.println("Enter cost of this question");
        listOfCosts.add(scanner.nextInt());
        listOfPossibleAnswers.add(bufferedList); //порядковый номер ответа == номер вопроса


        System.out.println("Enter number of the answer");
        int answer = scanner.nextInt();
        scanner.nextLine();

        listOfAnswers.add(answer);
        listOfQuestions.add(question);


    }
}
