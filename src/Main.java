import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        Questions questions = new Questions();

        Testing testing = new Testing();
        ArrayList<Integer> shuffledIndexes = new ArrayList<>();

        while (true){
            System.out.println("Enter command...");
            switch (scanner.nextLine()) {
                case "/help":
                    System.out.println("/help - list of commands\n" +
                            "/add - add new question\n" +
                            "/start - start test");
                    break;
                case "/add":
                    questions.addQuestion();
                    break;
                case "/start":
                    for (int i = 0; i < questions.listOfQuestions.size(); i++) {
                        shuffledIndexes.add(i);
                    }
                    System.out.println("Enter number of questions");
                    int numberOfQuestions = scanner.nextInt();
                    Collections.shuffle(shuffledIndexes);
                    if (!(numberOfQuestions <= questions.listOfQuestions.size())){
                        System.out.println("Number of questions is bigger than size of list of questions");
                        scanner.nextLine();
                        break;
                    }

                    Instant starts = Instant.now();
                    Thread.sleep(10);

                    for (int i = 0; i < numberOfQuestions; i++) {
                        System.out.println(questions.listOfQuestions.get(shuffledIndexes.get(i)));
                        System.out.println(questions.listOfPossibleAnswers.get(shuffledIndexes.get(i)));
                        System.out.println("Which one is correct? (0-n)");
                        int answerOfStudent = scanner.nextInt();
                        testing.areAnswersMatch(answerOfStudent, shuffledIndexes.get(i), questions);
                    }
                    Instant ends = Instant.now();
                    System.out.println("Your score is " + Testing.points);
                    System.out.println("Your time is " + Duration.between(starts, ends));

                    if (testing.indexesOfWrongAnswered.size() > 0){
                    System.out.println("You made mistakes in this questions:");
                        for (int i = 0; i < testing.indexesOfWrongAnswered.size(); i++) {
                            System.out.println(questions.listOfQuestions.get(i));
                            System.out.println("The right answer is");
                            System.out.println(questions.listOfAnswers.get(i));
                        }
                    }else{
                        System.out.println("All answers are correct!");
                    }
                    scanner.nextLine();
                    break;
                default:
                    System.out.println("Incorrect command");
                    break;
            }
        }

    }
}
