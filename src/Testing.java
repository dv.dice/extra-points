import java.util.ArrayList;

public class Testing {
    public static int points;
    ArrayList<Integer> indexesOfWrongAnswered = new ArrayList<>();
    public void areAnswersMatch(int answer, int index, Questions questions){

        if (answer == questions.listOfAnswers.get(index)){
            System.out.println("Right answer");
            points += questions.listOfCosts.get(index);
        }else{
            System.out.println("Wrong answer");
            indexesOfWrongAnswered.add(index);
        }
    }
}
